import logging

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

logging.basicConfig(
    level=logging.INFO,
    filename='garbanzoBot.log',
    format='%(asctime)s.%(msecs)03d %(levelname)s %(thread)s %(module)s:%(lineno)d - %(funcName)s: %(message)s'
)
if not logging.getLogger().hasHandlers():
    logging.getLogger().addHandler(logging.StreamHandler())

import os
from configparser import ConfigParser
import time

if not os.path.exists("settings.ini"):
    os.chdir('/home/pi/GarbanzoBot/')

settingsfile = ConfigParser()
settingsfile.read("settings.ini")
settings = settingsfile['CONFIG']
settingsDiscord = settingsfile['DISCORD']


def init():
    logging.info("Initializing web driver")
    try:
        options = webdriver.FirefoxOptions()
        options.headless = True
        profile = webdriver.FirefoxProfile(settings['firefox_profile'])
        profile.set_preference("dom.webdriver.enabled", False)
        profile.set_preference('useAutomationExtension', False)
        profile.update_preferences()
        desired = DesiredCapabilities.FIREFOX
        driver = webdriver.Firefox(executable_path=settings['firefox_executable'], desired_capabilities=desired,
                                   firefox_profile=profile, options=options)
    except Exception as e:
        logging.exception(e)
    return driver


def goto(driver, url):
    driver.get(url)


def getToken(url):
    driver = init()
    logging.info("Going to " + url)
    goto(driver, url)
    time.sleep(int(settings['firefox_wait']))
    driver.save_full_page_screenshot("goto.png")
    email = driver.find_element(By.CSS_SELECTOR, "[data-email=\"garbanzobot69@gmail.com\"]")
    email.click()
    logging.info("Email clicked")
    time.sleep(int(settings['firefox_wait']))
    driver.save_full_page_screenshot("email.png")
    advanced = driver.find_element(By.XPATH, "/html/body/div[1]/div[1]/a")
    advanced.click()
    logging.info("Advanced clicked")
    time.sleep(int(settings['firefox_wait']))
    driver.save_full_page_screenshot("advanced.png")
    unsafe = driver.find_element(By.XPATH, "/html/body/div[1]/div[2]/p[2]/a")
    unsafe.click()
    logging.info("unsafe clicked")
    time.sleep(int(settings['firefox_wait']))
    driver.save_full_page_screenshot("unsafe.png")
    inputs = driver.find_elements(By.CSS_SELECTOR, "input")
    for input in inputs:
        input.click()
        logging.info(str(input.get_attribute("aria-labelledby")) + " clicked")
    time.sleep(1)
    driver.save_full_page_screenshot("input.png")
    cont = driver.find_element(By.XPATH,
                               "/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div/div[2]/div/div/button/span")
    cont.click()
    logging.info("Continue clicked")
    time.sleep(int(settings['firefox_wait']))
    driver.save_full_page_screenshot("cont.png")
    text = driver.find_element(By.XPATH,
                               "/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section[3]/div/div/div/span/div/textarea")
    token_text = text.text
    logging.info("TOKEN = " + token_text)
    driver.save_full_page_screenshot("text.png")
    driver.close()
    return token_text
