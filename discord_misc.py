import logging

logging.basicConfig(
    level=logging.INFO,
    filename='garbanzoBot.log',
    format='%(asctime)s.%(msecs)03d %(levelname)s %(thread)s %(module)s:%(lineno)d - %(funcName)s: %(message)s'
)
if not logging.getLogger().hasHandlers():
    logging.getLogger().addHandler(logging.StreamHandler())

from discord_webhook import DiscordWebhook
from configparser import ConfigParser

settingsfile = ConfigParser()
settingsfile.read("settings.ini")
settings = settingsfile['CONFIG']
settingsDiscord = settingsfile['DISCORD']


def sendDiscordLog(msg):
    webhook = DiscordWebhook(url=settings["webhook_API"])
    webhook.content = msg
    webhook.execute()
