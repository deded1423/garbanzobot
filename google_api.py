import logging

import web_driver

logging.basicConfig(
    level=logging.INFO,
    filename='garbanzoBot.log',
    format='%(asctime)s.%(msecs)03d %(levelname)s %(thread)s %(module)s:%(lineno)d - %(funcName)s: %(message)s'
)
if not logging.getLogger().hasHandlers():
    logging.getLogger().addHandler(logging.StreamHandler())

import os
import re
import socket
from configparser import ConfigParser
import time
import platform
from async_misc import asyncio_run, ThreadWithReturnValue
from discord_misc import sendDiscordLog

from google.auth.transport.requests import Request
from google.auth.exceptions import RefreshError, TransportError
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build

if not os.path.exists("settings.ini"):
    os.chdir('/home/pi/GarbanzoBot/')

settingsfile = ConfigParser()
settingsfile.read("settings.ini")
settings = settingsfile['CONFIG']
settingsDiscord = settingsfile['DISCORD']


def getCreds(scopes):
    creds = None
    logging.info("getCreds()")
    logging.info(platform.system())
    flow = InstalledAppFlow.from_client_secrets_file(
        'credentials.json', scopes)
    flow.redirect_uri = 'urn:ietf:wg:oauth:2.0:oob'
    url, _ = flow.authorization_url()

    try:
        code = web_driver.getToken(url)
        flow.fetch_token(code=code)
        logging.info("Fetch OK!")
        creds = flow.credentials
        return creds
    except Exception as e:
        sendDiscordLog("Automatic token error ")
        logging.exception(e)
    sendDiscordLog(url)
    while True:
        time.sleep(1)
        if os.path.exists("code.out"):
            f = open("code.out", "r")
            logging.info("Read")
            code = f.readline()
            logging.info("Line" + str(code))
            try:
                flow.fetch_token(code=code)
                logging.info("Fetch OK!")
                creds = flow.credentials
                del f
                try:
                    os.remove("code.out")
                except:
                    pass
                break
            except Exception as e:
                sendDiscordLog("Incorrect Token!!!")
                logging.info(e)

            try:
                os.remove("code.out")
            except:
                pass
    return creds


def get_credentials():
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.

    scopes = 'https://www.googleapis.com/auth/script.scriptapp', \
             'https://www.googleapis.com/auth/script.projects.readonly', \
             'https://www.googleapis.com/auth/script.deployments.readonly', \
             'https://www.googleapis.com/auth/spreadsheets', \
             'https://www.googleapis.com/auth/script.external_request', \
             'https://www.googleapis.com/auth/drive.file', \
             'https://www.googleapis.com/auth/script.storage', \
             'https://www.googleapis.com/auth/docs', \
             'https://www.googleapis.com/auth/drive.metadata'

    if os.path.exists('token.json'):
        creds = Credentials.from_authorized_user_file('token.json', scopes)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            logging.info("Creds expired")
            try:
                creds.refresh(Request())
            except (RefreshError, TransportError):
                logging.info("Creds expired")
                creds = getCreds(scopes)
        else:
            logging.info("Creds expired")
            creds = getCreds(scopes)
        # Save the credentials for the next run
        with open('token.json', 'w') as token:
            token.write(creds.to_json())

        logging.info("Creds OK")
        sendDiscordLog(str(len(creds.scopes)) + " Expiring in " + str(creds.expiry))
    logging.info(str(len(creds.scopes)) + " Expiring in " + str(creds.expiry))
    return creds


def update_sheet(client, spreadsheet_id, range_cells, body):
    logging.info("Update_sheet spreadsheet_id range_cells " + str(range_cells) + " body " + str(body))
    try:
        service = build('sheets', 'v4', credentials=get_credentials())
    except:
        DISCOVERY_SERVICE_URL = 'https://sheets.googleapis.com/$discovery/rest?version=v4'
        service = build('sheets', 'v4', credentials=get_credentials(), discoveryServiceUrl=DISCOVERY_SERVICE_URL)

    response = service.spreadsheets().values() \
        .update(spreadsheetId=spreadsheet_id, range=range_cells, valueInputOption='RAW', body=body).execute()
    logging.info(response)


def swap_token(client, spreadsheet_id, token):
    body = {
        'values': [[token]],
    }
    update_sheet(client, spreadsheet_id, 'E9', body)


def get_token(spreadsheet_id):
    tokens = settings['token_api']

    logging.info("Get_token spreadsheet_id range_cells E9")
    try:
        service = build('sheets', 'v4', credentials=get_credentials())
    except:
        DISCOVERY_SERVICE_URL = 'https://sheets.googleapis.com/$discovery/rest?version=v4'
        service = build('sheets', 'v4', credentials=get_credentials(), discoveryServiceUrl=DISCOVERY_SERVICE_URL)

    response = service.spreadsheets().values() \
        .get(spreadsheetId=spreadsheet_id, range="E9").execute()
    logging.info("Response: " + str(response))
    if "values" not in response:
        last = None
    else:
        last = response["values"][0][0]
    found = False

    if last not in tokens.split(';'):
        return tokens.split(';')[0]

    for token in tokens.split(';'):
        if last is None:
            return token
        if last == token:
            found = True
        if found is True:
            return token
    return None


async def run_function(script_name, service, script_id, new_token, client, spreadsheet_id, message, rpb, command):
    if rpb:
        logging.info("Run " + script_name + " RPB")
    else:
        logging.info("Run " + script_name + " CLA")
    request = {"function": script_name, "parameters": [spreadsheet_id]}
    response = service.scripts().run(body=request, scriptId=script_id).execute()
    if 'error' in response:
        # The API executed, but the script returned an error.

        # Extract the first (and only) set of error details. The values of
        # this object are the script's 'errorMessage' and 'errorType', and
        # an list of stack trace elements.
        error = response['error']['details'][0]
        logging.info(script_name + " - Script error message: {0}".format(error['errorMessage']))

        if error['errorType'] == 'ScriptError' and (
                "returned code 401" in error['errorMessage'] or "returned code 429" in error['errorMessage']):
            logging.info("Getting new token")
            if new_token:
                return
            return await run_sheet(client, script_id, spreadsheet_id, message, rpb, command, True)

        if message is not None:
            await message.author.send("Error in log " + script_name + " " + str(rpb))
            await message.author.send("Script error message: {0}".format(error['errorMessage']))
            pass

        if 'scriptStackTraceElements' in error:
            # There may not be a stacktrace if the script didn't start
            # executing.
            logging.info("Script error stacktrace:")
            for trace in error['scriptStackTraceElements']:
                logging.info("\t{0}: {1}".format(trace['function'],
                                                 trace['lineNumber']))
    else:
        logging.info("Script " + script_name + " OK! " + str(rpb))


async def run_sheet(client, script_id, spreadsheet_id, message, rpb, command, new_token=False):
    logging.info("Run script_id " + script_id)
    socket.setdefaulttimeout(600)

    if new_token:
        logging.info("NEW TOKEN!!!! " + script_id)
        token = get_token(spreadsheet_id)
        logging.info("Old token: " + token + " " + script_id)
        if token is None:
            logging.error("No more tokens!!!")
            return
        swap_token(client, spreadsheet_id, token)

    service = build('script', 'v1', credentials=get_credentials())
    if rpb:
        # RPB
        await run_function("getData", service, script_id, new_token, client, spreadsheet_id, message, rpb, command)
        await run_function("setAllRoles", service, script_id, new_token, client, spreadsheet_id, message, rpb, command)
    else:
        # CLA
        await run_function("populateGearIssues", service, script_id, new_token, client, spreadsheet_id, message, rpb,
                           command)
        await run_function("populateDrumsEffectiveness", service, script_id, new_token, client, spreadsheet_id, message,
                           rpb, command)
        await run_function("populateValidate", service, script_id, new_token, client, spreadsheet_id, message, rpb,
                           command)
        await run_function("populateGearBreakdown", service, script_id, new_token, client, spreadsheet_id, message, rpb,
                           command)
        await run_function("populateBuffConsumables", service, script_id, new_token, client, spreadsheet_id, message,
                           rpb, command)
        await run_function("populateShadowResistance", service, script_id, new_token, client, spreadsheet_id, message,
                           rpb, command)
        await run_function("populateFightsMH", service, script_id, new_token, client, spreadsheet_id, message, rpb,
                           command)
        await run_function("populateFightsBT", service, script_id, new_token, client, spreadsheet_id, message, rpb,
                           command)
        await run_function("populateFightsSWP", service, script_id, new_token, client, spreadsheet_id, message, rpb,
                           command)

    logging.info("Run exportSheets " + str(rpb))
    request = {"function": "exportSheets", "parameters": [spreadsheet_id]}
    response = service.scripts().run(body=request, scriptId=script_id).execute()

    if 'error' in response:
        # The API executed, but the script returned an error.

        # Extract the first (and only) set of error details. The values of
        # this object are the script's 'errorMessage' and 'errorType', and
        # an list of stack trace elements.
        error = response['error']['details'][0]
        logging.error("exportSheets - Script error message: {0}".format(error['errorMessage']))

        if error['errorType'] == 'ScriptError' and (
                "returned code 401" in error['errorMessage'] or "returned code 429" in error['errorMessage']):
            logging.info("Getting new token " + str(rpb))
            if new_token:
                return
            return await run_sheet(client, script_id, spreadsheet_id, message, rpb, command, True)

        if message is not None:
            await message.author.send("Error in log")
            await message.author.send("Script error message: {0}".format(error['errorMessage']))
            pass

        if 'scriptStackTraceElements' in error:
            # There may not be a stacktrace if the script didn't start
            # executing.
            logging.info("Script error stacktrace:")
            for trace in error['scriptStackTraceElements']:
                logging.error("\t{0}: {1}".format(trace['function'],
                                                  trace['lineNumber']))

    else:
        logging.info("Script OK! " + str(rpb))
        newsheet_id = response["response"].get("result")
        logging.info("Moving file!" + str(newsheet_id) + " " + str(rpb))
        time.sleep(1)
        drive_service = build('drive', 'v3', credentials=get_credentials())

        file = drive_service.files().get(fileId=newsheet_id,
                                         fields='parents').execute()
        previous_parents = ",".join(file.get('parents'))

        drive_service.files().update(fileId=newsheet_id,
                                     addParents=settingsDiscord["drive_folder"],
                                     removeParents=previous_parents,
                                     fields='id, parents').execute()
        logging.info("End log! " + str(rpb))
        rest = drive_service.files().get(fileId=newsheet_id, fields="webViewLink, name").execute()
        file_link = rest["webViewLink"]
        newsheet_name = rest["name"]

        if not command:
            link = message.embeds[0].url
        else:
            m = re.search("^.log (?P<pre>https:\/\/(es\.)?classic.warcraftlogs.com\/reports\/)?(?P<code>[\w\d]+)(\/)?",
                          message.content)
            if m.group("pre") is None:
                link = m.group("pre") + m.group("code")
                pass
            else:
                link = "https://classic.warcraftlogs.com/reports/" + m.group("code")
                pass
        return newsheet_name, file_link, link
    pass


async def run_sheets(client, message, command):
    logging.info("Starting logs!")

    # asyncio.gather(run_sheet(client, settingsDiscord["script_ID_CLA"], message, False, command),
    #                run_sheet(client, settingsDiscord["script_ID_RPB"], message, True, command))

    # cla = asyncio_run(run_sheet(client, settingsDiscord["script_ID_CLA"], message, False, command))
    # rpb = asyncio_run(run_sheet(client, settingsDiscord["script_ID_RPB"], message, True, command))
    # loop = asyncio.new_event_loop()

    # cla = loop.run_until_complete(run_sheet(client, settingsDiscord["script_ID_CLA"], message, False, command))

    # loop = asyncio.new_event_loop()

    # rpb = loop.run_until_complete(run_sheet(client, settingsDiscord["script_ID_RPB"], message, True, command))

    # cla.result()
    # logging.info("CLA Done!")
    # rpb.result()
    # logging.info("RPB Done!")

    # await run_sheet(client, settingsDiscord["script_ID_CLA"], message, False, command)
    # await run_sheet(client, settingsDiscord["script_ID_RPB"], message, True, command)

    def cla():
        logging.info("CLA ID: " + settingsDiscord["script_ID_CLA"])
        return asyncio_run(
            run_sheet(client, settingsDiscord["script_ID_CLA"],
                      settingsDiscord["drive_CLA"], message, False, command))

    def rpb():
        logging.info("RPB ID: " + settingsDiscord["script_ID_RPB"])
        return asyncio_run(
            run_sheet(client, settingsDiscord["script_ID_RPB"],
                      settingsDiscord["drive_RPB"], message, True, command))

    cla = ThreadWithReturnValue(name="Cla", target=cla)
    rpb = ThreadWithReturnValue(name="Rpb", target=rpb)

    cla.start()
    rpb.start()
    cla_result = cla.join()
    logging.info("CLA Done!")
    rpb_result = rpb.join()
    logging.info("RPB Done!")
    return cla_result, rpb_result
