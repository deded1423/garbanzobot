import logging

logging.basicConfig(
    level=logging.INFO,
    filename='garbanzoBot.log',
    format='%(asctime)s.%(msecs)03d %(levelname)s %(thread)s %(module)s:%(lineno)d - %(funcName)s: %(message)s'
)
if not logging.getLogger().hasHandlers():
    logging.getLogger().addHandler(logging.StreamHandler())

import re
from configparser import ConfigParser

settingsfile = ConfigParser()
settingsfile.read("settings.ini")
settings = settingsfile['CONFIG']
settingsDiscord = settingsfile['DISCORD']

commandConfig = ConfigParser()
commandConfig.read("command.ini")


# Procesa un comando enviado por !command
async def process_command(message):
    add_command_RegEx = re.compile("^!addcommand (?P<name>[\w\d_]+) (?P<value>.+)?")
    delete_command_RegEx = re.compile("^!deletecommand (?P<name>[\w\d_]+)")

    guild_id = "GUILD_" + str(message.guild.id)
    if not commandConfig.has_section(guild_id):
        commandConfig.add_section(guild_id)
        with open("command.ini", 'w') as configfile:
            commandConfig.write(configfile)

    if message.content == "!garbanzohelp":
        logging.info("garbanzohelp " + " " + str(message.author.name))
        quote = "!log <url> | !commands | !commands | !addcommand <name> <quote> | !deletecommand <name>"
        await message.channel.send(quote)
        return True
    if message.content == "!commands" or message.content == "!gcommands":
        logging.info("commands ")

        commands = commandConfig[guild_id]
        quote = "Commands:\n"
        for command in commands.items():
            quote = quote + "!" + str(command[0]) + " "

        await message.delete()
        await message.channel.send(quote[:-1])
        return True

    # ADD COMMAND
    if add_command_RegEx.match(message.content) is not None:
        logging.info("Add command " + message.content + " " + str(message.author.name))
        found = False
        if message.author.permissions_in(message.channel).administrator or message.author.id == int(
                settings["master_user_id"]):
            found = True
        else:
            for role in message.author.roles:
                if role.name == "Officer" or role.name == "Oficial" or role.name == "Alpacria":
                    found = True
        logging.info("Found " + str(found))
        await message.delete()

        m = add_command_RegEx.search(message.content)
        name = m.group("name")
        value = m.group("value")
        commands = commandConfig[guild_id]

        commandConfig.set(guild_id, name, value)
        with open("command.ini", 'w') as configfile:
            commandConfig.write(configfile)

        return True

    # DELETE COMMAND
    if delete_command_RegEx.match(message.content) is not None:
        logging.info("Delete command " + message.content + " " + str(message.author.name))
        found = False
        if message.author.permissions_in(message.channel).administrator or message.author.id == int(
                settings["master_user_id"]):
            found = True
        else:
            for role in message.author.roles:
                if role.name == "Officer" or role.name == "Oficial":
                    found = True
        logging.info("Found " + str(found))
        await message.delete()

        m = delete_command_RegEx.search(message.content)
        name = m.group("name")

        commandConfig.remove_option(guild_id, name)
        with open("command.ini", 'w') as configfile:
            commandConfig.write(configfile)

        return True

    commands = commandConfig[guild_id]
    for command in commands.items():
        if "!" + command[0] == message.content:
            # await message.delete()
            await message.channel.send(command[1], reference=message)
            return True
    return False
