import logging
import random

import aram

logging.basicConfig(
    level=logging.INFO,
    filename='garbanzoBot.log',
    format='%(asctime)s.%(msecs)03d %(levelname)s %(thread)s %(module)s:%(lineno)d - %(funcName)s: %(message)s'
)
logging.getLogger().addHandler(logging.StreamHandler())
logger = logging.getLogger()
print = logging.info

import os
import re
import time
from configparser import ConfigParser
from datetime import datetime

import threading
import discord

import commands
import log
from discord_misc import sendDiscordLog

if not os.path.exists("settings.ini"):
    os.chdir('/home/pi/GarbanzoBot/')

# cfg = config.cfg

settingsfile = ConfigParser()
settingsfile.read("settings.ini")
settings = settingsfile['CONFIG']
settingsDiscord = settingsfile['DISCORD']


# Muestra la ayuda
async def show_help(client, message):
    await message.author.send("Help: \n" +
                              ".config NAME VALUE\n" +
                              ".config log_channel - Channel to get we hookup logs\n" +
                              ".config out_channel - Webhook to the channel to post logs\n" +
                              ".config drive_folder VALUE - Folder to move the logs\n" +
                              ".config drive_CLA VALUE - Id of the sheet in Google Docs\n" +
                              ".config drive_RPB VALUE - Id of the sheet in Google Docs\n" +
                              ".config script_ID_CLA VALUE - Id of the script in Apps Script\n" +
                              ".config script_ID_RPB VALUE - Id of the script in Apps Script\n" +
                              ".config range_CLA VALUE - Range of report in the sheet\n" +
                              ".config range_RPC VALUE - Range of report in the sheet\n")


# Procesa los comandos de configuracion
async def config(client, message, name, value):
    guild_id = message.guild.id
    if name == 'log_channel':
        found = False
        for channel in message.guild.channels:
            if message.channel.id == channel.id:
                found = True

        if found:
            settingsDiscord["log_channel_" + str(guild_id)] = str(message.channel.id)
            settingsDiscord["last_report_" + str(guild_id)] = datetime.utcnow().strftime(settings["date_format"])
            value = str(message.channel.id)
        pass
    elif name.lower() == 'out_channel'.lower():
        if value is None:
            message.author.send(".config out_channel VALUE")
            return
        settingsDiscord["out_channel_" + str(guild_id)] = str(value)
        pass
    elif name.lower() == 'drive_folder'.lower():
        if value is None:
            message.author.send(".config drive_folder VALUE")
            return
        settingsDiscord["drive_folder"] = str(value)
        pass
    elif name.lower() == 'drive_CLA'.lower():
        if value is None:
            message.author.send(".config drive_CLA VALUE")
            return
        settingsDiscord["drive_CLA"] = str(value)
        pass
    elif name.lower() == 'drive_RPB'.lower():
        if value is None:
            message.author.send(".config drive_RPB VALUE")
            return
        settingsDiscord["drive_RPB"] = str(value)
        pass
    elif name.lower() == 'script_ID_CLA'.lower():
        if value is None:
            message.author.send(".config script_ID_CLA VALUE")
            return
        settingsDiscord["script_ID_CLA"] = str(value)
        pass
    elif name.lower() == 'script_ID_RPB'.lower():
        if value is None:
            message.author.send(".config script_ID_RPB FOLDER")
            return
        settingsDiscord["script_ID_RPB"] = str(value)
        pass
    elif name.lower() == 'range_CLA'.lower():
        if value is None:
            message.author.send(".config range_CLA VALUE")
            return
        settingsDiscord["range_CLA"] = str(value)
        pass
    elif name.lower() == 'range_RPB'.lower():
        if value is None:
            message.author.send(".config range_RPB VALUE")
            return
        settingsDiscord["range_RPB"] = str(value)
        pass
    elif name.lower() == 'list'.lower():
        msg = "NAME = VALUE" + "\n"
        try:
            msg = msg + ("log_channel = " + settingsDiscord["log_channel_" + str(guild_id)] + "\n")
        except KeyError:
            msg = msg + "log_channel = NOT SET" + "\n"
        try:
            msg = msg + ("out_channel = " + settingsDiscord["out_channel_" + str(guild_id)] + "\n")
        except KeyError:
            msg = msg + "out_channel = NOT SET" + "\n"
        try:
            msg = msg + ("last_report = " + settingsDiscord["last_report_" + str(guild_id)] + "\n")
        except KeyError:
            msg = msg + "last_report = NOT SET" + "\n"
        try:
            msg = msg + ("drive_folder = " + settingsDiscord["drive_folder"] + "\n")
        except KeyError:
            msg = msg + "drive_folder = NOT SET" + "\n"
        try:
            msg = msg + ("drive_cla = " + settingsDiscord["drive_cla"] + "\n")
        except KeyError:
            msg = msg + "drive_cla = NOT SET" + "\n"
        try:
            msg = msg + ("drive_rpb = " + settingsDiscord["drive_rpb"] + "\n")
        except KeyError:
            msg = msg + "drive_rpb = NOT SET" + "\n"
        try:
            msg = msg + ("script_id_cla = " + settingsDiscord["script_id_cla"] + "\n")
        except KeyError:
            msg = msg + "script_id_cla = NOT SET" + "\n"
        try:
            msg = msg + ("script_id_rpb = " + settingsDiscord["script_id_rpb"] + "\n")
        except KeyError:
            msg = msg + "script_id_rpb = NOT SET" + "\n"
        try:
            msg = msg + ("range_cla = " + settingsDiscord["range_cla"] + "\n")
        except KeyError:
            msg = msg + "range_cla = NOT SET" + "\n"
        try:
            msg = msg + ("range_rpb = " + settingsDiscord["range_rpb"] + "\n")
        except KeyError:
            msg = msg + "range_rpb = NOT SET" + "\n"
        await message.author.send(msg)
        await message.delete()
        return
    else:
        await show_help(client, message)
        await message.delete()
        return
    await message.author.send("Config updated " + name + " - " + value)
    with open('settings.ini', 'w') as conf:
        settingsfile.write(conf)

    await message.delete()


# Envia un whisper con los ultimos logs de la app
async def send_log(client, message):
    try:
        f = open("garbanzoBot.log", "r")
        lines = f.readlines()
        log_text = "".join(lines[-40:])
        f.close()

        logging.info("Lenght = " + str(len(log_text)))

        n = 1999
        chunks = [log_text[i:i + n] for i in range(0, len(log_text), n)]
        logging.info("chunks Len = " + str(len(chunks)))
        for chunk in chunks:
            logging.info("chunk Len = " + str(len(chunk)))
            await message.author.send(chunk)
    except Exception as e:
        logging.exception(e)


def install_and_import(package):
    import importlib
    try:
        importlib.import_module(package)
    except ImportError:
        import pip
        pip.main(['install', package])
    finally:
        globals()[package] = importlib.import_module(package)


# Inicia el proceso del restart semanal
def restart_weekly():
    logging.info("Weekly restart set!!!")
    time.sleep(604800)
    logging.info("RESTART!!!")
    os.system("sudo supervisorctl restart GarbanzoBot")


async def respond_roll(client, message, number):
    await message.channel.send(random.randint(1, number), reference=message)
    pass


if __name__ == '__main__':

    sendDiscordLog("Starting...")
    log_command_RegEx = re.compile(
        "^.log (?P<pre>https:\/\/(es\.)?classic\.warcraftlogs\.com\/reports\/)?(?P<code>[\w\d]+)(\/)?")
    url_RegEx = re.compile(
        "(?P<pre>https:\/\/(es\.)?classic\.warcraftlogs\.com\/reports\/)?(?P<code>[\w\d]+)(\/)?")
    config_command_RegEx = re.compile("^.config (?P<name>[\w\d_]+)( )?(?P<value>.+)?")
    change_names_command_RegEx = re.compile("^!todos (?P<name>[\w\d_]+)")
    reload_command_RegEx = re.compile("^.reload")
    aram_command_RegEx = re.compile("^.aram (?P<number>[\d]+)")
    roll_command_RegEx = re.compile("^.roll (?P<number>[\d]+)")

    intents = discord.Intents.default()
    intents.members = True
    intents.presences = True
    client = discord.Client(intents=intents)

    if os.path.exists('code.out'):
        os.remove("code.out")
    if os.path.exists('token.json'):
        token_time = datetime.utcnow() - datetime.fromtimestamp(os.path.getmtime('token.json'))
        logging.info("Now time: " + str(datetime.utcnow()))
        logging.info("Token time: " + str(datetime.fromtimestamp(os.path.getmtime('token.json'))))
        logging.info("Days difference: " + str(token_time.days))
        if token_time.days > 0:
            os.remove("token.json")
        pass


    @client.event
    async def on_ready():
        logging.info('We have logged in as {0.user}'.format(client))
        for guild in client.guilds:
            logging.info("Guild " + str(guild.name) + " - Id: " + str(guild.id))
            logging.info("Reading messages...")
            await reload_messages(guild)


    async def reload_messages(guild):
        if not settingsfile.has_option("DISCORD", "log_channel_" + str(guild.id)):
            settingsfile.set("DISCORD", "log_channel_" + str(guild.id), "")
            settingsfile.set("DISCORD", "out_channel_" + str(guild.id), "")
            settingsfile.set("DISCORD", "last_report_" + str(guild.id), "")
            with open("settings.ini", 'w') as configfile:
                settingsfile.write(configfile)
        if settingsDiscord["log_channel_" + str(guild.id)] != '':
            channel = guild.get_channel(int(settingsDiscord["log_channel_" + str(guild.id)]))
            messages = await channel.history(
                after=datetime.strptime(settingsDiscord["last_report_" + str(guild.id)], settings["date_format"])
            ).flatten()
            logging.info(datetime.utcnow().strftime(settings["date_format"]))
            logging.info("Messages: " + str(len(messages)))
            for message in messages:
                logging.info("Content: " + str(message.content) + " Author: " + str(message.author))
                await on_message(message)


    global last_id
    last_id = 0


    @client.event
    async def on_message(message):
        global last_id
        if message.author == client.user:
            return
        if message.guild is None:
            if message.content == "bot1423" and not str(message.author.id) in settings["master_user_id"]:
                settings["master_user_id"] = settings["master_user_id"] + "," + str(message.author.id)
                with open('settings.ini', 'w') as conf:
                    settingsfile.write(conf)
            if str(message.author.id) in settings["master_user_id"]:
                if message.content == "reboot":
                    logging.info("REBOOT!!!")
                    sendDiscordLog("REBOOT!!!")
                    os.system("sudo reboot")
                if message.content == "restart":
                    logging.info("RESTART!!!")
                    sendDiscordLog("RESTART!!!")
                    os.system("sudo supervisorctl restart garbanzoBot")
                if message.content == "stop":
                    logging.info("STOP!!!")
                    sendDiscordLog("STOP!!!")
                    os.system("sudo supervisorctl stop garbanzoBot")
                if message.content == "log":
                    logging.info("LOG!!!")
                    await send_log(client, message)
                if "token" in message.content:
                    logging.info("TOKEN!!!")
                    token = message.content.replace("token ", "")
                    f = open("code.out", "w")
                    f.write(token)
                if message.content == "reset_time":
                    for key in list(settingsDiscord.keys()):
                        if "last_report" in key:
                            settingsDiscord[key] = datetime.utcnow().strftime(settings["date_format"])

                    with open('settings.ini', 'w') as conf:
                        settingsfile.write(conf)
                if config_command_RegEx.match(message.content) is not None:
                    m = config_command_RegEx.search(message.content)
                    if m.group("name").lower() == "drive_folder":
                        if m.group("value") is None:
                            message.author.send(".config drive_folder VALUE")
                            return
                        for key in list(settingsDiscord.keys()):
                            if "drive_folder" in key:
                                settingsDiscord[key] = m.group("value")
                    if m.group("name").lower() == "drive_cla":
                        if m.group("value") is None:
                            message.author.send(".config drive_cla VALUE")
                            return
                        for key in list(settingsDiscord.keys()):
                            if "drive_cla" in key:
                                settingsDiscord[key] = m.group("value")
                    if m.group("name").lower() == "drive_rpb":
                        if m.group("value") is None:
                            message.author.send(".config drive_rpb VALUE")
                            return
                        for key in list(settingsDiscord.keys()):
                            if "drive_rpb" in key:
                                settingsDiscord[key] = m.group("value")
                    if m.group("name").lower() == "script_id_cla":
                        if m.group("value") is None:
                            message.author.send(".config script_id_cla VALUE")
                            return
                        for key in list(settingsDiscord.keys()):
                            if "script_id_cla" in key:
                                settingsDiscord[key] = m.group("value")
                    if m.group("name").lower() == "script_id_rpb":
                        if m.group("value") is None:
                            message.author.send(".config script_id_rpb VALUE")
                            return
                        for key in list(settingsDiscord.keys()):
                            if "script_id_rpb" in key:
                                settingsDiscord[key] = m.group("value")

                    with open('settings.ini', 'w') as conf:
                        settingsfile.write(conf)
                    await message.author.send("UPDATED!!!")

            return

        if last_id == message.id:
            last_id = message.id
            return
        last_id = message.id
        logging.info("Channel " + str(message.channel) + str(message.channel.name))
        logging.info("Author " + str(message.author) + str(message.author.name))
        logging.info("Message " + message.content)

        if message.channel.id == int(settings["channel_API"]) and str(message.author.id) in settings["master_user_id"]:
            logging.info("TOKEN!!!")
            token = message.content.replace("token ", "")
            f = open("code.out", "w")
            f.write(token)

        guild_id = message.guild.id
        if len(message.embeds) != 0 and message.channel.id == int(settingsDiscord["log_channel_" + str(guild_id)]):
            logging.info("Embedded " + message.embeds[0].url)
            logging.info("Embedded " + message.channel.id)

            m = url_RegEx.search(message.embeds[0].url)
            await log.add_to_log(message, m.group("pre"), m.group("code"), False)
        # Mirar el canal donde lo postea y hacer un comando para eso

        if config_command_RegEx.match(message.content) is not None:
            found = False
            if message.author.permissions_in(message.channel).administrator or str(message.author.id) in settings[
                "master_user_id"]:
                found = True
            else:
                for role in message.author.roles:
                    if role.name == "Officer" or role.name == "Oficial":
                        found = True
            if not found:
                await message.delete()
                return

            m = config_command_RegEx.search(message.content)
            await config(client, message, m.group("name"), m.group("value"))
            return

        if change_names_command_RegEx.match(message.content) is not None:
            # Comando para cambiar los nombres
            logging.info("Change names " + message.content + " " + str(message.author.name))
            found = False
            if message.author.permissions_in(message.channel).administrator or str(message.author.id) in settings[
                "master_user_id"]:
                found = True
            else:
                for role in message.author.roles:
                    if role.name == "Officer" or role.name == "Oficial" or role.name == "Alpacria":
                        found = True
            logging.info("Found " + str(found))
            await message.delete()

            m = change_names_command_RegEx.search(message.content)
            name = m.group("name")
            user_id = message.author.id

            for channel in client.get_all_channels():
                if isinstance(channel, discord.VoiceChannel):
                    for member in channel.members:
                        if member.id == user_id:
                            for member in channel.members:
                                logging.info("Change nick " + str(member.name))
                                try:
                                    await member.edit(nick=name)
                                except discord.Forbidden:
                                    logging.info("Cant change nick " + str(member.name))
                                    pass

            return

        if log_command_RegEx.match(message.content) is not None:
            # if no hay canal de salida avisar
            if settingsDiscord["out_channel_" + str(guild_id)] == '':
                await message.author.send("No output channel (\".config output_channel\" on the appropriate channel)")
                return

            m = log_command_RegEx.search(message.content)
            await log.add_to_log(message, m.group("pre"), m.group("code"), True)

        if reload_command_RegEx.match(message.content) is not None:
            logging.info("Reloading " + str(message.channel.guild))
            await message.delete()
            await reload_messages(message.channel.guild)
            return

        if aram_command_RegEx.match(message.content):
            logging.info("Aram!!! " + str(message.channel.guild))
            m = aram_command_RegEx.search(message.content)
            number = m.group("number")
            await aram.print_aram(client, message, int(number))
            return

        if roll_command_RegEx.match(message.content):
            logging.info("Roll!!! " + str(message.channel.guild))
            m = roll_command_RegEx.search(message.content)
            number = m.group("number")
            await respond_roll(client, message, int(number))
            return

        if await commands.process_command(message):
            logging.info("Command processed " + message.content)
            return


    threading.Thread(target=log.log_worker, daemon=True, args=(client,)).start()
    threading.Thread(target=restart_weekly, daemon=True).start()
    client.run(settings["token"])
