import logging

from discord_misc import sendDiscordLog

logging.basicConfig(
    level=logging.INFO,
    filename='garbanzoBot.log',
    format='%(asctime)s.%(msecs)03d %(levelname)s %(thread)s %(module)s:%(lineno)d - %(funcName)s: %(message)s'
)
if not logging.getLogger().hasHandlers():
    logging.getLogger().addHandler(logging.StreamHandler())

import time
from configparser import ConfigParser

import asyncio
import queue
from async_misc import asyncio_run
import discord
from discord_webhook import DiscordWebhook, DiscordEmbed
from googleapiclient import errors

settingsfile = ConfigParser()
settingsfile.read("settings.ini")
settings = settingsfile['CONFIG']
settingsDiscord = settingsfile['DISCORD']

import google_api

log_queue = queue.Queue()


# https://discord.com/api/oauth2/authorize?client_id=886252168343068692&permissions=536964096&scope=bot
# Procesa y hace el log de logs
async def log(client, message, pre, code, command):
    guild_id = message.guild.id
    ret = None
    if pre is None:
        pre = ""
    logging.info(
        "Log " + pre + " Code: " + code + " Channel: " + str(message.channel) + " User: " + str(message.author))
    try:
        # CLA
        body = {
            'values': [[code]],
        }
        logging.info("update_sheet CLA")
        google_api.update_sheet(client, settingsDiscord["drive_CLA"],
                                settingsDiscord["range_CLA"], body)
        logging.info("update_sheet CLA Post")
        # RPB
        body = {
            'values': [[code]],
        }
        logging.info("update_sheet RPB")
        google_api.update_sheet(client, settingsDiscord["drive_RPB"],
                                settingsDiscord["range_RPB"], body)
        logging.info("update_sheet RPB Post")
        logging.info("run_sheets")
        ret = await google_api.run_sheets(client, message, command)
        logging.info("run_sheets RPB Post")

    except errors.HttpError as e:
        # The API encountered a problem before the script started executing.
        logging.info(e.content)

    settingsDiscord["last_report_" + str(guild_id)] = message.created_at.strftime(settings["date_format"])
    with open('settings.ini', 'w') as conf:
        settingsfile.write(conf)
    return ret


# Añade el log de logs a la cola del worker

async def add_to_log(message, pre, code, command):
    logging.info(
        "Add to log " + pre + " Code: " + code + " Channel: " + str(message.channel) + " User: " + str(message.author))
    if not command:
        logging.info("Sleeping for 60 sec for " + str(code))
        time.sleep(1)
    logging.info("Adding: " + str(code))
    bundle = (message, pre, code, command)
    log_queue.put(bundle)

    logging.info("Deleting... " + code)
    if command:
        await message.delete()


# Inicia el worker
def log_worker(client):
    logging.info("Get tokens")
    google_api.get_credentials()
    logging.info("Done tokens")

    logging.info("Starting log_worker")
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    while True:
        bundle = log_queue.get()
        logging.info("Dequeue " + bundle[2])
        sendDiscordLog("Log: " + str(bundle[2]))

        asyncio_run(
            client.change_presence(activity=discord.Activity(name=bundle[2], type=discord.ActivityType.playing)))

        try:
            drives = asyncio.run(log(client, bundle[0], bundle[1], bundle[2], bundle[3]), debug=True)
            logging.info("POST asyncio_run " + str(drives))
            if drives is not None or drives[0] is not None or drives[1] is not None:

                logging.info("Creating embed all")
                if settings["out_channel_all"] != settingsDiscord["out_channel_" + str(bundle[0].guild.id)]:
                    webhook = DiscordWebhook(url=settings["out_channel_all"])

                    embed = DiscordEmbed(title=str(drives[0][0]).replace("CLA for ", ""), color='03b2f8')
                    embed.set_url(drives[0][2])

                    embed.set_timestamp()

                    embed.add_embed_field(name='CLA', value="[" + drives[0][1] + "](" + drives[0][1] + ")")
                    embed.add_embed_field(name='RPB', value="[" + drives[1][1] + "](" + drives[1][1] + ")")

                    # add embed object to webhook
                    webhook.add_embed(embed)

                    response = webhook.execute()

                    logging.info(response)

                logging.info("Creating embed " + str(drives[0]) + " " + str(drives[1]))
                webhook = DiscordWebhook(url=settingsDiscord["out_channel_" + str(bundle[0].guild.id)])

                # create embed object for webhook
                embed = DiscordEmbed(title=str(drives[0][0]).replace("CLA for ", ""), color='03b2f8')
                embed.set_url(drives[0][2])

                embed.set_timestamp()

                embed.add_embed_field(name='CLA', value="[" + drives[0][1] + "](" + drives[0][1] + ")")
                embed.add_embed_field(name='RPB', value="[" + drives[1][1] + "](" + drives[1][1] + ")")

                # add embed object to webhook
                webhook.add_embed(embed)

                response = webhook.execute()

                logging.info(response)

                #
            else:
                logging.error("No embed created")

        except Exception as e:
            sendDiscordLog("Error in log " + str(bundle[2]) + " " + str(e))
            logging.exception(str(e))

        logging.info("Finished!")
        asyncio_run(client.change_presence(activity=None))
        log_queue.task_done()
