function exportSheets() {
  ret = generateRoleSheets("All")
  return ret
}
function getData() {
  generateAllSheet("All")
}

function setAllRoles() {
  var nameSheet = "All";
  var ss = SpreadsheetApp.openById('1u9964HKos7R7qEG6CFEvVbyiLvaMNQ-yDrApSzW2wP4');
  var sheet = ss.getSheetByName(nameSheet);
  var baseSheetName = sheet.getName();

  var data = sheet.getDataRange().getValues();
  var lastRow = -1;
  for (var i = 0; i < data.length; i++) {
    if (data[i][1] == "seconds active on single target") { //[1] because column B
      lastRow = i;
    }
  }
  if (lastRow == -1) {
    return
  }
  var names = null;
  var _class = null;
  var uses = null;
  for (var i = 0; i < data[6].length; i++) {
    if (data[6][i] == "Druids" || data[6][i] == "Hunters" || data[6][i] == "Mages" || data[6][i] == "Paladins" || data[6][i] == "Priests" || data[6][i] == "Rogues" || data[6][i] == "Shamans" || data[6][i] == "Warlocks" || data[6][i] == "Warriors") {
      names = getArray(sheet, lastRow, i + 1);
      _class = data[6][i];
      continue;
    }
    if (names == null || data[6][i] == "") {
      continue;
    }
    uses = getArray(sheet, lastRow, i + 1, sheet);

    //Druids
    if (_class == "Druids") {
      var melee = getUses(names, uses, "Melee");
      var starfire = getUses(names, uses, "Starfire");
      var lifebloom = getUses(names, uses, "Lifebloom");
      //HEALER
      if ((lifebloom > starfire && lifebloom > melee) || getUses(names, uses, "Tree of Life") > 0) {
        setRole(sheet, i + 1, "Healer");
        continue;
      }
      //CASTER
      if ((starfire > lifebloom && starfire > melee)) {
        setRole(sheet, i + 1, "Caster");
        continue;
      }
      //CAT
      if ((melee > starfire && melee > lifebloom) && getUses(names, uses, "Growl") == 0) {
        setRole(sheet, i + 1, "Physical");
        continue;
      }
      //TANK
      if ((melee > starfire && melee > lifebloom)) {
        setRole(sheet, i + 1, "Tank");
        continue;
      }
    }
    //Paladins
    if (_class == "Paladins") {
      //HEALER
      if (getUses(names, uses, "Holy Shock") > 0) {
        setRole(sheet, i + 1, "Healer");
        continue;
      }
      //PHYSICAL
      if (getUses(names, uses, "Crusader Strike") > 0) {
        setRole(sheet, i + 1, "Physical");
        continue;
      }
      //TANK
      if (getUses(names, uses, "Holy Shield") > 0 || getUses(names, uses, "Avenger's Shield") > 0) {
        setRole(sheet, i + 1, "Tank");
        continue;
      }

    }
    //Priests
    if (_class == "Priests") {
      //HEALER
      if (getUses(names, uses, "Circle of Healing") > 0 || getUses(names, uses, "Holy Nova") > 0) {
        setRole(sheet, i + 1, "Healer");
        continue;
      }
      //CASTER
      if (getUses(names, uses, "Vampiric Embrace") > 0 || getUses(names, uses, "Vampiric Touch") > 0) {
        setRole(sheet, i + 1, "Caster");
        continue;
      }
    }
    //Shamans
    if (_class == "Shamans") {
      var melee = getUses(names, uses, "Melee");
      var bolt = getUses(names, uses, "Lightning Bolt");
      var wave = getUses(names, uses, "Chain Heal");
      //HEALER
      if ((wave > bolt && wave > melee) || getUses(names, uses, "Earth Shield") > 0) {
        setRole(sheet, i + 1, "Healer");
        continue;
      }
      //CASTER
      if ((bolt > wave && bolt > melee) || getUses(names, uses, "Elemental Mastery") > 0 || getUses(names, uses, "Totem of Wrath") > 0) {
        setRole(sheet, i + 1, "Caster");
        continue;
      }
      //PHYSICAL
      if ((melee > bolt && melee > wave) || getUses(names, uses, "Stormstrike") > 0) {
        setRole(sheet, i + 1, "Physical");
        continue;
      }

    }
    //Warriors
    if (_class == "Warriors") {
      //PHYSICAL
      if (getUses(names, uses, "Death Wish") > 0||getUses(names, uses, "Bloodthirst") > 0||getUses(names, uses, "Mortal Strike") > 0||getUses(names, uses, "Slam", false) > 0) {
        setRole(sheet, i + 1, "Physical");
        continue;
      }
      //TANK
      if (getUses(names, uses, "Shield Slam") > 0||getUses(names, uses, "Revenge") > 0) {
        setRole(sheet, i + 1, "Tank");
        continue;
      }

    }
  }
}
var numberReg = new RegExp("(\\d) \\(\\d*%\\)", "g");
function getUses(names, uses, name, exact=true) {
  var number = -1;
  if (names != null && names.length > 0 && names.length == uses.length) {
    for (var i = 0, j = names.length; i < j; i++) {
      if ((names[i][0].toUpperCase().includes(name.toUpperCase()) && exact)||(names[i][0].toUpperCase() == name.toUpperCase() && !exact)) {
        var n = uses[i][0];
        if (number == -1) {
          number = 0;
        }
        if ((typeof n === 'string' || n instanceof String) && n.includes(" ")) {
          number = number + parseInt(n.split(" ")[0]);
        } else {
          number = number + parseInt(n);
        }
      }
    }
  }
  return number;
}

function getArray(sheet, lastRow, column) {
  var range = sheet.getRange(9, column, lastRow - 9);
  return range.getValues();
}

function setRole(sheet, column, role) {
  sheet.getRange(5, column).setValue(role);
  //Logger.log("Set " + sheet.getRange(7, column).getValues() + " with " + role);
}